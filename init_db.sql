CREATE TABLE urls
(
    id TEXT PRIMARY KEY NOT NULL,
    original_url TEXT NOT NULL,
    short_suffix TEXT NOT NULL,
    creation_time TIMESTAMPTZ NOT NULL
);