package ru.sber.url.data;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Basic entity of Url in the database
 */
@Entity
@Table(name = "urls")
@Getter
@Setter
public class UrlEntry {

    @Id
    private @NotNull String id;

    @Column
    private @NotNull String originalUrl;

    @Column
    private @NotNull String shortSuffix;

    @Column
    private @NotNull LocalDateTime creationTime;

    public UrlEntry(
            @NotNull String id,
            @NotNull String originalUrl,
            @NotNull String shortSuffix,
            @NotNull LocalDateTime creationTime
    ) {
        this.id = id;
        this.originalUrl = originalUrl;
        this.shortSuffix = shortSuffix;
        this.creationTime = creationTime;
    }

    public UrlEntry() {}
}
