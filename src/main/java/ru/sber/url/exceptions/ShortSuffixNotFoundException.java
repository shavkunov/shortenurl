package ru.sber.url.exceptions;

/**
 * Thrown when short url cannot be found in the database
 */
public class ShortSuffixNotFoundException extends Exception {
}
