package ru.sber.url.exceptions;

/**
 * Thrown when url is expired
 */
public class UrlExpiredException extends Exception {
}
