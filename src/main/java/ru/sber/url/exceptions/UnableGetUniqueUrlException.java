package ru.sber.url.exceptions;

/**
 * Thrown when short url cannot be created.
 */
public class UnableGetUniqueUrlException extends Exception {
}
