package ru.sber.url.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.url.data.UrlEntry;

/**
 * JPA repository for sending basic queries to the database.
 */
public interface ShortUrlRepository extends JpaRepository<UrlEntry, Long> {

    /**
     * Find entity by it's short url
     * @param shortSuffix provided short url
     * @return null if entity not found and desired entity otherwise
     */
    @Nullable
    UrlEntry findByShortSuffix(@NotNull String shortSuffix);

    /**
     * Find entity by it's original url
     * @param originalUrl provided original url
     * @return null if entity not found and desired entity otherwise
     */
    @Nullable
    UrlEntry findByOriginalUrl(@NotNull String originalUrl);

}