package ru.sber.url.service;

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.url.data.UrlEntry;
import ru.sber.url.exceptions.ShortSuffixNotFoundException;
import ru.sber.url.exceptions.UnableGetUniqueUrlException;
import ru.sber.url.exceptions.UrlExpiredException;
import ru.sber.url.repository.ShortUrlRepository;

import java.time.*;

@Service
public class ShortUrlService {

    @Autowired
    private final @NotNull ShortUrlRepository urlRepository;

    private static final int shortUrlSize = 8;
    private static final int expiringMinuteDuration = 10;

    public ShortUrlService(@NotNull ShortUrlRepository urlRepository) {
        this.urlRepository = urlRepository;
    }

    /**
     * Basic operation of obtaining SHA3 512 bits hash.
     * @param input string where hash will be computed
     * @return string representation of the SHA3 hash.
     */
    public static @NotNull String getSha3Digest(@NotNull String input) {
        SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest512();
        byte[] digest = digestSHA3.digest(input.getBytes());
        return Hex.toHexString(digest);
    }

    /**
     * Computing short url based on SHA3 hash for the provided url
     * @param longURL input url considered to be long
     * @return short version creted suffix
     * @throws UnableGetUniqueUrlException thrown when different windows of the computed hash is already existing in
     * the database. In this case, more bits in hash should be added or the main strategy of
     * picking fixed length window should be reconsidered.
     */
    public @NotNull String getShortUrl(@NotNull String longURL) throws UnableGetUniqueUrlException {
        UrlEntry possibleResult = urlRepository.findByOriginalUrl(longURL);
        if (possibleResult != null) {
            return possibleResult.getShortSuffix();
        }

        String sha3Hash = getSha3Digest(longURL);

        int leftBorder = 0;
        int rightBorder = shortUrlSize;

        String shortUrlCandidate;

        boolean condition;
        do {
            shortUrlCandidate = sha3Hash.substring(leftBorder, rightBorder);
            leftBorder++;
            rightBorder++;

            condition = urlRepository.findByShortSuffix(shortUrlCandidate) != null;
        } while (condition && rightBorder < sha3Hash.length());

        if (condition && rightBorder == sha3Hash.length()) {
            throw new UnableGetUniqueUrlException();
        }

        urlRepository.save(new UrlEntry(
                sha3Hash,
                longURL,
                shortUrlCandidate,
                LocalDateTime.now(ZoneOffset.UTC))
        );

        return shortUrlCandidate;
    }

    /**
     * Obtain full Url by provided short version url
     * @param shortSuffix provided short suffix, which should exist in the database
     * @return full url
     * @throws ShortSuffixNotFoundException thrown, when short suffix not found in the database
     * @throws UrlExpiredException thrown, when url is expired 10 minutes interval
     */
    public @NotNull String getFullUrl(
            @NotNull String shortSuffix) throws ShortSuffixNotFoundException, UrlExpiredException {
        UrlEntry entity = urlRepository.findByShortSuffix(shortSuffix);
        if (entity != null) {
            Duration duration = Duration.between(entity.getCreationTime(), LocalDateTime.now(ZoneOffset.UTC));
            if (duration.toMinutes() >= expiringMinuteDuration) {
                throw new UrlExpiredException();
            }

            return entity.getOriginalUrl();
        }

        throw new ShortSuffixNotFoundException();
    }
}
