package ru.sber.url.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;
import ru.sber.url.exceptions.ShortSuffixNotFoundException;
import ru.sber.url.exceptions.UnableGetUniqueUrlException;
import ru.sber.url.exceptions.UrlExpiredException;
import ru.sber.url.service.ShortUrlService;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Spring Rest Controller for REST API.
 */
@RestController
public class ShortenUrlController {

    @Autowired
    private ShortUrlService service;

    /**
     * Handling errors for all types of operations and show error message to user.
     * @param exception exception to be shown
     * @return Response containing error message and http status
     */
    @ExceptionHandler(ResponseStatusException.class)
    public @NotNull ResponseEntity<String> handleException(@NotNull ResponseStatusException exception) {
        return ResponseEntity
                .status(exception.getStatus())
                .body(exception.getMessage());
    }

    /**
     * Endpoint for creating short urls.
     * @param longUrl input url that considered to be long
     * @return converted short version of the url.
     * @throws ResponseStatusException throws when user provided either invalid url or hash could not be created
     */
    @PostMapping("/short_url")
    public @NotNull String addShortUrl(
            @RequestParam(value="longUrl") @NotNull String longUrl) throws ResponseStatusException {
        try {
            new URL(longUrl);
            return service.getShortUrl(longUrl);
        } catch (MalformedURLException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid URL, please check it carefully.");
        } catch (UnableGetUniqueUrlException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to create tiny url hash.");
        }
    }

    /**
     * Endpoint for obtaining full version of short url
     * @param shortSuffix short url, existing in the database from addShortUrl Endpoint
     * @return full version url
     * @throws ResponseStatusException throws when short version url could not be found in the database
     * or when url is expired
     */
    @GetMapping("/get_full_url")
    public @NotNull String getFullUrl(
            @RequestParam(value="shortSuffix") @NotNull String shortSuffix) throws ResponseStatusException {
        try {
            return service.getFullUrl(shortSuffix);
        } catch (ShortSuffixNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No short url found in database.");
        } catch (UrlExpiredException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Url is expired");
        }
    }

    /**
     * Endpoint for redirection to the full url destination
     * @param shortSuffix short url, existing in the database from addShortUrl Endpoint
     * @return redirection to the destination page
     * @throws ResponseStatusException throws when short version url could not be found in the database
     * or when url is expired
     */
    @GetMapping("/s/{shortSuffix}")
    @ResponseBody
    public @NotNull RedirectView redirect(@PathVariable @NotNull String shortSuffix) throws ResponseStatusException {
        try {
            String fullUrl = service.getFullUrl(shortSuffix);
            return new RedirectView(fullUrl);
        } catch (ShortSuffixNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No short url found in database.");
        } catch (UrlExpiredException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Url is expired");
        }
    }
}
