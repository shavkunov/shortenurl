package ru.sber.url;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.sber.url.controller.ShortenUrlController;
import ru.sber.url.repository.ShortUrlRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTests {
    private static final String longUrl = "https://en.wikipedia.org/wiki/SHA-3";

    @Autowired
    private ShortenUrlController controller;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    public void shortUrlTest() throws Exception {
        String expectedShortUrl = "bd41edae";
        MvcResult postResult = this.mockMvc.perform(MockMvcRequestBuilders
                .post("/short_url").param("longUrl", longUrl)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String actualShortUrl = postResult.getResponse().getContentAsString();
        assertEquals(expectedShortUrl, actualShortUrl);

        MvcResult getResult = this.mockMvc.perform(MockMvcRequestBuilders
                .get("/get_full_url").param("shortSuffix", actualShortUrl)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        assertEquals(longUrl, getResult.getResponse().getContentAsString());
    }

}
