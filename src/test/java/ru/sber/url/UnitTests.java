package ru.sber.url;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sber.url.data.UrlEntry;
import ru.sber.url.exceptions.ShortSuffixNotFoundException;
import ru.sber.url.exceptions.UnableGetUniqueUrlException;
import ru.sber.url.exceptions.UrlExpiredException;
import ru.sber.url.repository.ShortUrlRepository;
import ru.sber.url.service.ShortUrlService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class UnitTests {
    private static UrlEntry oldEntry;
    private static UrlEntry newEntry;
    private static final String longUrl = "https://en.wikipedia.org/wiki/SHA-3";

    @Mock
    private ShortUrlRepository repoMock = Mockito.mock(ShortUrlRepository.class);

    @BeforeAll
    static void initUrl() {
        String hash = ShortUrlService.getSha3Digest(longUrl);
        LocalDateTime oldDate = Instant.ofEpochMilli(0L).atZone(ZoneOffset.UTC).toLocalDateTime();
        oldEntry = new UrlEntry(hash, longUrl, "bd41edae", oldDate);
        newEntry = new UrlEntry(hash, longUrl, "bd41edae", LocalDateTime.now());
    }

    @Test
    void testUnitUrlShortening() throws UnableGetUniqueUrlException {
        when(repoMock.save(oldEntry)).thenReturn(null);

        ShortUrlService service = new ShortUrlService(repoMock);

        Assertions.assertEquals(oldEntry.getShortSuffix(), service.getShortUrl(longUrl));
    }

    @Test
    void testUnitUrlFull() throws UnableGetUniqueUrlException, ShortSuffixNotFoundException, UrlExpiredException {
        when(repoMock.save(newEntry)).thenReturn(null);

        ShortUrlService service = new ShortUrlService(repoMock);
        String actualShortUrl = service.getShortUrl(longUrl);

        when(repoMock.findByShortSuffix(actualShortUrl)).thenReturn(newEntry);

        String actualFullUrl = service.getFullUrl(actualShortUrl);

        Assertions.assertEquals(newEntry.getOriginalUrl(), actualFullUrl);

        assertThrows(ShortSuffixNotFoundException.class, () -> service.getFullUrl("test"));
    }

    @Test
    void testExpiredException() throws UnableGetUniqueUrlException, UrlExpiredException, ShortSuffixNotFoundException {
        when(repoMock.save(oldEntry)).thenReturn(null);

        ShortUrlService service = new ShortUrlService(repoMock);
        String actualShortUrl = service.getShortUrl(longUrl);

        when(repoMock.findByShortSuffix(actualShortUrl)).thenReturn(oldEntry);

        assertThrows(UrlExpiredException.class, () -> service.getFullUrl(actualShortUrl));
    }

}
