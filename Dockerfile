FROM openjdk:11
ADD build/libs/url-0.0.1-SNAPSHOT.jar url.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "url.jar"]